package membership

import (
  "log"
  "testing"
)

func TestBasic(t *testing.T) {
  Basic(3)
  Basic(4)
  err := Proof()
  if err != nil {
    log.Printf("Oh no", err)
    t.Fail()
  }
}
