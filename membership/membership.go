
package membership

import (
  "errors"
  "log"

  "crypto/rand"
  "math/big"
  "golang.org/x/crypto/bn256"
)

/********
 * Implements proof from paper:
 *   "Efficient Protocols for Set Membership
 *        and Range Proofs"
 *   Camenisch, Chaaouni, and shelat
 *   ASIACRYPT 2008
 */

func Basic(a int) int {
  return a + 2
}

func Proof() error {
  // Shared: 
  // g1, h1, C = g1^sigma h1^r, 
  // set of valid elements

  // WARNING!!!! THIS IS TOTALLY BOGUS INSECURE
  // JUST FOR DEMONSTRATION PURPOSES!!!!
  // No apparent way to hash into G1/G2 yet
  _, g1, err := bn256.RandomG1(rand.Reader)
  if err != nil {
    return err
  }

  _, g2, err := bn256.RandomG2(rand.Reader)
  if err != nil {
    return err
  }

  _, h1, err:= bn256.RandomG1(rand.Reader)
  if err != nil {
    return err
  }

  sigma := big.NewInt(8)
  sigma_idx := 4
  r, err := rand.Int(rand.Reader, bn256.Order)
  if err != nil {
    return err
  }

  C := commitGH(g1, sigma, h1, r)

  // V sends to P
  //    y = g2^x , x random in Z_p
  //    for each i:
  //      A_i = g1^{1/(x+i)}     [in G1]
  x, y, err := bn256.RandomG2(rand.Reader)
  if err != nil {
    return err
  }

  bitlen := bn256.Order.BitLen()
  A_values := make([]*bn256.G1, bitlen)
  for i := 0; i < bitlen; i++ {
    x_i := new(big.Int)
    if i > 0 {
      x_i.SetBit(big.NewInt(0), i-1, 1)
    }

    log.Printf("x_i", i, " ", x_i)
    x_i.Add(x_i, x)
    x_i.Mod(x_i, bn256.Order)
    x_i.ModInverse(x_i, bn256.Order)
    A_values[i] = new(bn256.G1).ScalarMult(g1, x_i)
  }

  // P sends to V
  //    v random in Z_p
  //    V = A_sigma^v           [in G1]
  v, _, err := bn256.RandomG1(rand.Reader)
  if err != nil {
    return err
  }

  V := new(bn256.G1).ScalarMult(A_values[sigma_idx], v)

  // P and V run the external PoK proto
  log.Printf("Must insert extra test code!!!!")
  log.Printf("Not fully implemented yet!!!!")

  // P sends to V
  //    s, t, m random in Z_p
  //    a = e(V, g2)^{-s} e(g1, g2)^t
  //    D = g1^s h1^m
  s, err := rand.Int(rand.Reader, bn256.Order)
  if err != nil {
    return err
  }

  t, err := rand.Int(rand.Reader, bn256.Order)
  if err != nil {
    return err
  }

  m, err := rand.Int(rand.Reader, bn256.Order)
  if err != nil {
    return err
  }

  a := pairToTheX(V, g2, negativeExp(s))
  a.Add(a, pairToTheX(g1, g2, t))
  log.Printf("a''", a)

  // XXX remove this test code
  /*
  gg := pairToTheX(g1, g2, t)
  a_test := new(bn256.GT).Add(a, pairToTheX(V, g2, s))
  if a_test.String() != gg.String() {
    log.Printf("> ", a_test)
    log.Printf(">  ", gg)
    return errors.New("not matching")
  }
  */

  D := commitGH(g1, s, h1, m)

  // V sends to P
  //    c random in Z_p
  c, err := rand.Int(rand.Reader, bn256.Order)
  if err != nil {
    return err
  }

  c = big.NewInt(1)

  // P sends to V
  //    z_sigma = s - sigma*c
  z_sigma := aMinusBCmodQ(s, sigma, c, bn256.Order)
  //    z_v = t - vc
  z_v := aMinusBCmodQ(t, v, c, bn256.Order)
  //    z_r = m - rc
  z_r := aMinusBCmodQ(m, r, c, bn256.Order)

  log.Printf("z_sigma %v", z_sigma)
  log.Printf("s       %v", s)
  log.Printf("z_v     %v", z_v)
  log.Printf("t       %v", t)
  log.Printf("z_r     %v", z_r)
  log.Printf("m       %v", m)

  // Verifier checks
  //    D ?= C^c h1^{z_r} g1^{z_sigma}
  //    a ?= e(V, y)^c * e(V, g2)^{-z_sigma} * e(g1, g2)^{z_v}

  D_prime := new(bn256.G1).ScalarMult(g1, z_sigma)
  D_prime.Add(D_prime, commitGH(C, c, h1, z_r))

  if D.String() != D_prime.String() {
    log.Printf("D  %v", D)
    log.Printf("Dp %v", D_prime)
    return errors.New("D value did not check")
  }

  a_prime_1 := pairToTheX(V, y, c)
  log.Printf("ap1 %v", a_prime_1)
  a_prime_2 := pairToTheX(V, g2, negativeExp(z_sigma))
  log.Printf("ap2 %v", a_prime_2)
  a_prime_3 := pairToTheX(g1, g2, z_v)
  log.Printf("ap3 %v", a_prime_3)

  a_prime := new(bn256.GT).Add(new(bn256.GT).Add(a_prime_1, a_prime_2), a_prime_3)

  log.Printf("a  %v", a)
  log.Printf("ap %v", a_prime)
  if a.String() != a_prime.String() {
    return errors.New("\"a\" value did not check")
  }

  //...
  return nil
}

// g^a h^b
func commitGH(g *bn256.G1, a *big.Int, h *bn256.G1, b *big.Int) *bn256.G1 {
  g_a := new(bn256.G1).ScalarMult(g, a)
  h_b := new(bn256.G1).ScalarMult(h, b)
  prod := new(bn256.G1).Add(g_a, h_b)
  return prod
}

// a - b*c % q
func aMinusBCmodQ(a *big.Int, b *big.Int, c *big.Int, q *big.Int) *big.Int {
  bc := new(big.Int).Mul(b, c)
  a_minus_bc := new(big.Int).Sub(a, bc)
  a_minus_bc_mod_q := new(big.Int).Mod(a_minus_bc, q)
  return a_minus_bc_mod_q
}

func pairToTheX(g1 *bn256.G1, g2 *bn256.G2, x *big.Int) *bn256.GT {
  pair := bn256.Pair(g1, g2)
  pair = pair.ScalarMult(pair, x)
  return pair
}

func negativeExp(x *big.Int) *big.Int {
  return new(big.Int).Sub(bn256.Order, x)
}
