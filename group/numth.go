

package group

import (
  "errors"
  "math/big"
)

func SqRoot(a, p *big.Int) (*big.Int, error) {
  tmp := new(big.Int).Mod(p, big.NewInt(4))
  if tmp.Cmp(big.NewInt(3)) != 0 {
    return tmp, errors.New("Modulus p != 3 mod 4")
  }

  exp := tmp.Add(p, big.NewInt(1))
  exp.Div(exp, big.NewInt(4))
  return exp.Exp(a, exp, p), nil
}

func IsQuadRes(a, p *big.Int) *big.Int {
  exp := new(big.Int).Sub(p, big.NewInt(1))
  exp.Div(exp, big.NewInt(2))
  return exp.Exp(a, exp, p)
}

