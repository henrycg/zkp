
package group

import (
  "bytes"
  "fmt"
  "encoding/gob"
  "crypto/rand"
  "crypto/elliptic"
  "math/big"
)

var p256GeneratorH Element

func init() {
  // XXX THIS IS TOTALLY INSECURE. JUST USE
  // FOR PERFORMANCE TESTING!!!!!
  c := CurveP256()
  p256GeneratorH = c.Pow(c.GeneratorG(), big.NewInt(5))
}

type Element interface {
  // Abstract type
}

type CyclicGroup interface {
  Mul(a, b Element) (Element)

  Pow(a Element, exp *big.Int) (Element)

  PowG(exp *big.Int) (Element)

  Inverse(a Element) (Element)

  RandomExponent() (exp *big.Int)

  Order() (*big.Int)

  Identity() Element

  GeneratorG() Element

  GeneratorH() Element

  IsIdentity(a Element) (bool)

  RandomElement() (Element)

  AreEqual(a, b Element) bool

  String(a Element) string

  // Serialization
  Marshal(a Element) []byte
  Unmarshal(b []byte) Element
}

/*****

func Commit(g CyclicGroup, m *big.Int) (Element, *big.Int) {
  if g.Order().Cmp(m) < 0 {
    log.Fatal("Message is larger than group order")
  }

  rand := g.RandomExponent()
  commit := g.Mul(g.Pow(g.GeneratorG(), m),
  g.Pow(g.GeneratorH(), rand))
  return commit, rand
}
*/

/*****
 * Elliptic curve ops
 */

type CurveElement struct {
  x *big.Int
  y *big.Int
}

type CurvePreproc struct {
  gs []CurveElement
}

type CurveGroup struct {
  curve elliptic.Curve
}

func CurveP256() CyclicGroup {
  var c CurveGroup
  c.curve = elliptic.P256()
  return CyclicGroup(c)
}

func (c CurveGroup) Mul(a, b Element) Element {
  var r CurveElement
  aC := a.(CurveElement)
  bC := b.(CurveElement)
  r.x, r.y = c.curve.Add(aC.x, aC.y, bC.x, bC.y)
  return r
}

func (c CurveGroup) Pow(a Element, exp *big.Int) Element {
  elm := a.(CurveElement)
  var r CurveElement
  r.x, r.y = c.curve.ScalarMult(elm.x, elm.y, exp.Bytes())
  return Element(r)
}

func (c CurveGroup) PowG(exp *big.Int) Element {
  var r CurveElement
  r.x, r.y = c.curve.ScalarBaseMult(exp.Bytes())
  return Element(r)
}

func (c CurveGroup) Inverse(a Element) Element {
  negOrder := new(big.Int).Sub(c.Order(), big.NewInt(1))
  return Element(c.Pow(a, negOrder))
}

func (c CurveGroup) RandomExponent() (*big.Int) {
  exp, _ := rand.Int(rand.Reader, c.Order())
  return exp
}

func (c CurveGroup) RandomElement() Element {
  // XXX this is a bogus way to do this...
  return c.Pow(c.GeneratorG(), c.RandomExponent())
}

func (c CurveGroup) Order() (*big.Int) {
  return c.curve.Params().N
}

func (c CurveGroup) IsIdentity (a Element) bool {
  zero := big.NewInt(0)
  return (a.(CurveElement).x.Cmp(zero) == 0 &&
    a.(CurveElement).y.Cmp(zero) == 0)
}

func (c CurveGroup) Identity() Element {
  return c.Pow(c.GeneratorG(), big.NewInt(0))
}

func (c CurveGroup) GeneratorG() Element {
  var r CurveElement
  r.x = c.curve.Params().Gx
  r.y = c.curve.Params().Gy
  return r
}

func (c CurveGroup) GeneratorH() Element {
  return p256GeneratorH
}

func (c CurveGroup) AreEqual(a, b Element) bool {
  x := (a.(CurveElement).x.Cmp(b.(CurveElement).x)) == 0
  y := (a.(CurveElement).y.Cmp(b.(CurveElement).y)) == 0
  return x && y
}

func (c CurveGroup) String(a Element) string {
  return fmt.Sprintf("<%v,%v>",
    a.(CurveElement).x.String(),
    a.(CurveElement).y.String())
}

func (c CurveGroup) Marshal(a Element) []byte {
  return elliptic.Marshal(c.curve, a.(CurveElement).x, a.(CurveElement).y)
}

func (c CurveGroup) Unmarshal(b []byte) Element {
  var e CurveElement
  e.x, e.y = elliptic.Unmarshal(c.curve, b)
  return e
}

func (p CurveElement) MarshalBinary() ([]byte, error) {
  var b bytes.Buffer
  enc := gob.NewEncoder(&b)
  err := enc.Encode(p.x)
  if err != nil {
    return b.Bytes(), nil
  }
  err = enc.Encode(p.y)
  return b.Bytes(), err
}

// UnmarshalBinary modifies the receiver so it must take a pointer receiver.
func (p *CurveElement) UnmarshalBinary(data []byte) error {
  // A simple encoding: plain text.
  b := bytes.NewBuffer(data)
  dec := gob.NewDecoder(b)
  err := dec.Decode(&p.x)
  if err != nil {
    return err
  }

  return dec.Decode(&p.y)
}

func init() {
  gob.Register(CurveElement{})
}

