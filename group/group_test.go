package group

import (
  "crypto/rand"
  "math/big"
  "testing"

  "code.google.com/p/go.crypto/curve25519"
)

func getGroup() CyclicGroup {
  return CurveP256()
}


func TestBasic(t *testing.T) {
  group := getGroup()
  order := group.Order()
  for i := 0; i<10; i++ {
    g := group.RandomElement()
    h := group.RandomElement()
    m := group.RandomExponent()
    r := group.RandomExponent()

    zero := big.NewInt(0)
    if m.Cmp(zero) == -1 || m.Cmp(order) > -1 {
      t.Fail()
    }

    if group.AreEqual(g, h) || group.IsIdentity(g) || group.IsIdentity(h) {
      t.Fail()
    }

    if m == r {
      t.Fatal("m == r")
    }

    res := group.Mul(group.Pow(g, m), group.Pow(h,r))
    res2 := group.Mul(group.Pow(h, r), group.Pow(g,m))

    if !group.AreEqual(res, res2) {
      t.Fatal("Equality failed")
    }

    x := group.RandomElement()
    x2 := group.Mul(x, x)
    x4 := group.Mul(x2, x2)
    x5 := group.Mul(x4, x)
    if !group.AreEqual(x5, group.Pow(x, big.NewInt(5))) {
      t.Fatal("x^5 != x^5")
    }
  }
}

func TestInverse(t *testing.T) {
  group := getGroup()
  //order := group.Order()
  for i := 0; i<10; i++ {
    g := group.RandomElement()
    g_inv := group.Inverse(g)
    one := group.Mul(g, g_inv)
    if !group.IsIdentity(one) {
      t.Fail()
    }
  }
}

func TestPow(t *testing.T) {
  group := getGroup()
  for i := 0; i<10; i++ {
    g := group.RandomElement()
    a1 := group.RandomExponent()
    a2 := group.RandomExponent()
    a3 := group.RandomExponent()

    exp := new(big.Int)
    exp.Add(exp, a1)
    exp.Add(exp, a2)
    exp.Add(exp, a3)

    v1 := group.Pow(g, exp)
    v2 := group.Mul(group.Pow(g, a1), group.Pow(g, a2))
    v2 = group.Mul(v2, group.Pow(g, a3))


    if !group.AreEqual(v1, v2) {
      t.Fail()
    }

    v3 := group.PowG(exp)
    v4 := group.Pow(group.GeneratorG(), exp)
    if !group.AreEqual(v3, v4) {
      t.Fail()
    }
  }
}

func BenchmarkPow(b *testing.B) {
  group := getGroup()
  g := group.GeneratorG()
  b.ResetTimer()
  for i := 0; i<b.N; i++ {
    x := group.RandomExponent()
    _ = group.Pow(g, x)
  }
}

func BenchmarkMul(b *testing.B) {
  group := getGroup()
  g := group.GeneratorG()
  b.ResetTimer()
  for i := 0; i<b.N; i++ {
    _ = group.Mul(g, g)
  }
}

func Benchmark25519Pow(b *testing.B) {
  group := getGroup()
  var src, dst [32]byte
  for i := 0; i<b.N; i++ {
    x := group.RandomExponent()
    copy(src[:], x.Bytes())
    curve25519.ScalarBaseMult(&dst, &src)
  }
}

func Benchmark2048Mul(b *testing.B) {
  p_string := "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF"

  p := new(big.Int)
  p.SetString(p_string, 16)
  b.ResetTimer()

  for i := 0; i<b.N; i++ {
    a, _ := rand.Int(rand.Reader, p)
    b, _ := rand.Int(rand.Reader, p)
    a.Mul(a, b)
    a.Mod(a, p)
  }
}

func BenchmarkReduction(b *testing.B) {
  group := getGroup()
  q := group.Order()

  x, _ := rand.Int(rand.Reader, q)
  y, _ := rand.Int(rand.Reader, q)
  for i := 0; i<b.N; i++ {
    x.Add(x, y)
    x.Mod(x, q)
  }
}

