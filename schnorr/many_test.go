package schnorr

import (
	"testing"

	"bitbucket.org/henrycg/zkp/group"
)

func TestManySchnorrZero(t *testing.T) {
	testManySchnorrOnce(t, 0)
}

func TestManySchnorrNonzero(t *testing.T) {
	testManySchnorrOnce(t, 1)
}

func testManySchnorrOnce(t *testing.T, bogusIdx int) {
	group := group.CurveP256()
	var st ManyStatement
	var wit ManyWitness

	nProofs := 50

	st.GtoXs = make([]Statement, nProofs)
	wit.Xs = make([]Witness, nProofs)

	wit.BogusIdx = bogusIdx

	for i := 0; i < 50; i++ {
		st.GtoXs[i].G = group.RandomElement()
		wit.Xs[i].X = group.RandomExponent()
		st.GtoXs[i].X = group.Pow(st.GtoXs[i].G, wit.Xs[i].X)

	}

	wit.Xs[wit.BogusIdx].X = group.RandomExponent()

	ev := ManyProve(group, st, wit)

	if !ManyVerify(group, st, ev) {
		t.Fail()
	}
}

func TestManySchnorrBad(t *testing.T) {
	group := group.CurveP256()
	var st ManyStatement
	var wit ManyWitness

	nProofs := 50

	st.GtoXs = make([]Statement, nProofs)
	wit.Xs = make([]Witness, nProofs)

	wit.BogusIdx = 3

	for i := 0; i < 50; i++ {
		st.GtoXs[i].G = group.RandomElement()
		wit.Xs[i].X = group.RandomExponent()
		st.GtoXs[i].X = group.Pow(st.GtoXs[i].G, wit.Xs[i].X)
	}

	wit.Xs[(wit.BogusIdx+1)%nProofs].X = group.RandomExponent()

	ev := ManyProve(group, st, wit)

	if ManyVerify(group, st, ev) {
		t.Fail()
	}
}

/*
func TestManySchnorrVerify(t *testing.T) {
  group := getGroup()
  var st ManyStatement
  var wit ManyWitness

  nProofs := 1000

  st.GtoXs = make([]Statement, nProofs)
  wit.Xs = make([]Witness, nProofs)

  wit.BogusIdx = 0

  for i := 0; i<nProofs; i++ {
    st.GtoXs[i].G = group.RandomElement()
    wit.Xs[i].X = group.RandomExponent()
    st.GtoXs[i].X = group.Pow(st.GtoXs[i].G, wit.Xs[i].X)

    if (i == wit.BogusIdx) {
      wit.Xs[i].X = group.RandomExponent()
    }
  }

  ev := ManyProve(group, st, wit)

  ManyVerify(group, st, ev)
}
*/
