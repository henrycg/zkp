package schnorr

import (
  "math/big"
  "bitbucket.org/henrycg/zkp/group"
)

type Element group.Element
type CyclicGroup group.CyclicGroup

type Statement struct {
  G Element   // generator 
  X Element   // g^x
}

type Witness struct {
  X *big.Int // x in g^x
}

type ProofState struct {
  v *big.Int
  V Element
}

type Evidence struct {
  Commit Element
  Response *big.Int
}

func ProveStart(group CyclicGroup, st Statement, wit Witness) ProofState {
    var pf ProofState

    // V = g^v
    pf.v = group.RandomExponent()
    pf.V = group.Pow(st.G, pf.v)
    return pf
}

func (pf ProofState) ProveFinish(group CyclicGroup, wit Witness, challenge *big.Int) Evidence {
    // r = v - c*x
    cx := new(big.Int).Mul(challenge, wit.X)

    var ev Evidence
    ev.Commit = pf.V
    ev.Response = new(big.Int)
    ev.Response.Sub(pf.v, cx)
    ev.Response.Mod(ev.Response, group.Order())

    return ev
}

func Verify(group CyclicGroup, st Statement, ev Evidence, challenge *big.Int) bool {
  V := group.Mul(group.Pow(st.G, ev.Response), group.Pow(st.X, challenge))
  return group.AreEqual(ev.Commit, V)
}

func Forge(group CyclicGroup, st Statement, challenge *big.Int) Evidence {
  var ev Evidence
  ev.Response = group.RandomExponent()

  gToR := group.Pow(st.G, ev.Response)
  ev.Commit = group.Mul(gToR, group.Pow(st.X, challenge))
  return ev
}

