package schnorr


import (
  "crypto/sha256"
  "math/big"
//  "log"
)

type ManyStatement struct {
  GtoXs []Statement // g^(x_i)
}

type ManyWitness struct {
  Xs []Witness  // x_i in g^(x_i)
  BogusIdx int  // index of exponent we don't know
}

type ManyEvidence struct {
  Evs []Evidence
  Slope *big.Int
}

func ManyProve(group CyclicGroup, st ManyStatement, wit ManyWitness) ManyEvidence {
  nProofs := len(wit.Xs)

  var ev ManyEvidence
  ev.Evs = make([]Evidence, nProofs)

  bogusChal := group.RandomExponent()
  pfs := make([]ProofState, nProofs)

  // Compute commits for the real proofs
  for i := 0; i < nProofs; i++ {
    if (i == wit.BogusIdx) {
      // Forge the bogus proof
      ev.Evs[wit.BogusIdx] = Forge(group, st.GtoXs[wit.BogusIdx], bogusChal)
      pfs[i].V = ev.Evs[wit.BogusIdx].Commit
    } else {
      pfs[i] = ProveStart(group, st.GtoXs[i], wit.Xs[i])
    }
  }

  // Compute the challenge function
  chal := hashElements(group, st, pfs)

  // Pick the challenges such that:
  //    f(-1)      = chal = Hash(...)
  //    f(bogus)  = bogusChal
  // 
  //    f(i) = chal + (((bogusChal - chal)/BogusIdx) * i)

  intercept := chal
  q := group.Order()

  // bogusChal - chal
  delta := new(big.Int).Sub(bogusChal, chal)
  delta = delta.Mod(delta, q)

  // (bogusChal - chal)/BogusIdx
  ev.Slope = new(big.Int).ModInverse(big.NewInt(int64(wit.BogusIdx + 1)), q)
  ev.Slope = ev.Slope.Mul(ev.Slope, delta)
  ev.Slope = ev.Slope.Mod(ev.Slope, q)


  // Compute the rest of the proof
  for i := 0; i < nProofs; i++ {
    if (i == wit.BogusIdx) {
      continue
    }

    c_i := findChallenge(group, i, ev.Slope, intercept)
    ev.Evs[i] = pfs[i].ProveFinish(group, wit.Xs[i], c_i)
  }

  return ev
}

func ManyVerify(group CyclicGroup, st ManyStatement, ev ManyEvidence) bool {
  nProofs := len(st.GtoXs)

  // Recompute intercept
  pfs := make([]ProofState, nProofs)
  for i := 0; i < nProofs; i++ {
    pfs[i].V = ev.Evs[i].Commit
  }

  intercept := hashElements(group, st, pfs)
  for i := 0; i < nProofs; i++ {
  }

  for i := 0; i < nProofs; i++ {
    c_i := findChallenge(group, i, ev.Slope, intercept)
    if !Verify(group, st.GtoXs[i],  ev.Evs[i], c_i) {
      //log.Printf("Verify failed on item %v", i)
      return false
    }
  }

  return true
}

func hashElements(group CyclicGroup, st ManyStatement, pfs []ProofState) *big.Int {
    // c = Hash(g, g^x, g^v)
    hash := sha256.New()

    // XXX WARNING! This is not safe. Just for research
    // performance purposes. Do NOT use this sort of 
    // nonsense in production code.

    for i := 0; i<len(pfs); i++ {
      hash.Write([]byte("==g=="))
      //log.Printf(" g[%v] = %v", i, group.String(st.GtoXs[i].G))
      hash.Write(group.Marshal(st.GtoXs[i].G))
      hash.Write([]byte("==X=="))
      //log.Printf(" X[%v] = %v", i, group.String(st.GtoXs[i].X))
      hash.Write(group.Marshal(st.GtoXs[i].X))
      hash.Write([]byte("==V=="))
      //log.Printf(" V[%v] = %v", i, group.String(pfs[i].V))
      hash.Write(group.Marshal(pfs[i].V))
    }

    h := hash.Sum(nil)
    return new(big.Int).SetBytes(h)
}

func findChallenge(group CyclicGroup, idx int, Slope, intercept *big.Int) *big.Int {
  // return ax + b mod q
  q := group.Order()
  r := new(big.Int)
  r.Mul(Slope, big.NewInt(int64(idx+1)))
  r.Add(r, intercept)
  r.Mod(r, q)
  return r
}

