package schnorr

import (
	"testing"

	"bitbucket.org/henrycg/zkp/group"
)

func getGroup() CyclicGroup {
	return group.CurveP256()
}

func TestSchnorrGood(t *testing.T) {
	group := getGroup()
	for i := 0; i < 10; i++ {
		var st Statement
		var wit Witness
		wit.X = group.RandomExponent()

		st.G = group.RandomElement()
		st.X = group.Pow(st.G, wit.X)

		pf := ProveStart(group, st, wit)
		c := group.RandomExponent()
		ev := pf.ProveFinish(group, wit, c)
		if !Verify(group, st, ev, c) {
			t.Fatal("Valid proof rejected")
		}
	}
}

func TestSchnorrBad(t *testing.T) {
	group := getGroup()
	for i := 0; i < 10; i++ {
		var st Statement
		var wit Witness
		wit.X = group.RandomExponent()

		st.G = group.RandomElement()
		st.X = group.Pow(st.G, wit.X)

		pf := ProveStart(group, st, wit)
		c := group.RandomExponent()
		ev := pf.ProveFinish(group, wit, c)

		c.Add(c, group.RandomExponent())
		c.Mod(c, group.Order())
		if Verify(group, st, ev, c) {
			t.Fatal("Bogus challenge accepted")
		}

		ev = pf.ProveFinish(group, wit, c)
		ev.Response.Add(ev.Response, group.RandomExponent())
		ev.Response.Mod(ev.Response, group.Order())
		if Verify(group, st, ev, c) {
			t.Fatal("Bogus Response accepted")
		}
	}
}

func TestSchnorrForge(t *testing.T) {
	group := getGroup()
	for i := 0; i < 10; i++ {
		var st Statement
		var wit Witness
		wit.X = group.RandomExponent()

		st.G = group.RandomElement()
		st.X = group.Pow(st.G, wit.X)

		c := group.RandomExponent()
		ev := Forge(group, st, c)
		if !Verify(group, st, ev, c) {
			t.Fail()
		}
	}
}

func BenchmarkSchnorr(b *testing.B) {
	group := getGroup()
	var st Statement
	var wit Witness
	wit.X = group.RandomExponent()

	st.G = group.RandomElement()
	st.X = group.Pow(st.G, wit.X)

	for i := 0; i < b.N; i++ {

		pf := ProveStart(group, st, wit)
		c := group.RandomExponent()
		pf.ProveFinish(group, wit, c)
	}
}
