
package commit

import (
  "log"
  "math/big"

  "bitbucket.org/henrycg/zkp/group"
)

var curve = group.CurveP256()

type Plaintexts struct {
  Values []*big.Int
}

type Commits struct {
  Commits []group.Element
}

type Secrets struct {
  Secrets []*big.Int
}

func BoolToVector(bs []bool) Plaintexts {
  var p Plaintexts
  var v int64
  p.Values = make([]*big.Int, len(bs))
  for i:=0; i<len(bs); i++ {
    if bs[i] {
      v = 1
    } else {
      v = 0
    }

    p.Values[i] = big.NewInt(v)
  }

  return p
}

func Commit(m *big.Int) (group.Element, *big.Int) {
  if curve.Order().Cmp(m) < 0 {
    log.Fatal("Message is larger than group order")
  }

  rand := curve.RandomExponent()
  commit := curve.Mul(curve.Pow(curve.GeneratorG(), m),
                     curve.Pow(curve.GeneratorH(), rand))
  return commit, rand
}

func CommitToVector(bs Plaintexts) (Commits, Secrets) {
  var pub Commits
  var sec Secrets
  l := len(bs.Values)
  pub.Commits = make([]group.Element, l)
  sec.Secrets = make([]*big.Int, l)

  for i:=0; i<l; i++ {
    sec.Secrets[i] = curve.RandomExponent()
    pub.Commits[i] = curve.Mul(curve.Pow(curve.GeneratorG(), bs.Values[i]),
                       curve.Pow(curve.GeneratorH(), sec.Secrets[i]))
  }

  return pub, sec
}

func CommitDiff(bs Plaintexts, com Commits) Commits {
  l := len(bs.Values)
  if len(com.Commits) != l {
    log.Fatal("Vectors must have the same length")
  }

  var pub Commits
  pub.Commits = make([]group.Element, l)

  // Given g^a h^r  and b, 
  // compute 
  //    g^{a-b} h^r
  for i:=0; i<l; i++ {
    g_neg_b := curve.Pow(curve.GeneratorG(), bs.Values[i])
    g_neg_b = curve.Inverse(g_neg_b)
    pub.Commits[i] = curve.Mul(com.Commits[i], g_neg_b)
  }

  return pub
}
